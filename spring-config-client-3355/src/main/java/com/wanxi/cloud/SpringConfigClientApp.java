package com.wanxi.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringConfigClientApp {
    public static void main(String[] args) {
        SpringApplication.run(SpringConfigClientApp.class,args);
    }
}
