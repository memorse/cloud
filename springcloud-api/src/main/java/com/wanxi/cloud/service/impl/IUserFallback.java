package com.wanxi.cloud.service.impl;

import com.wanxi.cloud.pojo.User;
import com.wanxi.cloud.service.IUserService;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class IUserFallback implements IUserService {

    public List<User> getAllUser() {
        return new ArrayList<User>();
    }

    public User getUser(Integer id) {
        return new User()
                .setId(id)
                .setAge(0)
                .setName("no User with id: "+id+" in database")
                .setDeptID("none");
    }
}
