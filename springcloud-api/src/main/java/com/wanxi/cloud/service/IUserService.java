package com.wanxi.cloud.service;

import com.wanxi.cloud.pojo.User;
import com.wanxi.cloud.service.impl.IUserFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Component("iUserService")
@FeignClient(value = "USER-CLI",fallback = IUserFallback.class)
public interface IUserService {

    @GetMapping("/users")
    public List<User> getAllUser();

    @GetMapping("/user/{id}")
    public User getUser(@PathVariable("id") Integer id);
}
