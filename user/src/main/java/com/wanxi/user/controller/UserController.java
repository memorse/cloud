package com.wanxi.user.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import com.wanxi.cloud.pojo.User;
import com.wanxi.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserService service;
    @Value("${server.port}")
    private String port;

    @GetMapping("/users")
    public List<User> getAllUser(){
        System.out.printf(port);
        return service.getAllPerson();
    }

    @GetMapping("/user/{id}")
    @HystrixCommand(fallbackMethod ="getUserHystrix")
    public User getUser(@PathVariable("id") Integer id){
        User personById = service.getPersonById(id);
        if(personById==null){
            throw new RuntimeException("没有id为"+id+"的用户");
        }
        return personById;
    }

    public User getUserHystrix(@PathVariable("id") Integer id){
        return new User()
                    .setId(id)
                    .setName("no User with id"+id);
    }


}
