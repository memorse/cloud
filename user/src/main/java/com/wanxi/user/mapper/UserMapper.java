package com.wanxi.user.mapper;

import com.wanxi.cloud.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface UserMapper {
    @Select("select * from person where name=#{name}")
    User getPersonByName(String name);

    @Select("select * from person")
    List<User> getAllPerson();

    @Insert("insert into person(name,gender,age,phone,dept_id)" +
            "values (#{name},#{gender},#{age},#{phone},#{deptID}")
    Integer addPerson(User person);
    @Delete("delete from person where id=#{id}")
    Integer deletePerson(int id);

    @Update("update person set name=#{name},age=#{age},gender=#{gender},phone=#{phone} where id=#{id}")
    Integer updatePerson(User person);

    @Select("select * from person where id=#{id}")
    User getPersonById(Integer id);
}
