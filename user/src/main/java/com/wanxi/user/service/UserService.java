package com.wanxi.user.service;

import com.wanxi.cloud.pojo.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface UserService {
    User getPersonByName(String name);
    User getPersonById(Integer id);

    List<User> getAllPerson();

    Integer addPerson(User person);

    Integer deletePerson(int id);

    Integer updatePerson(User person);
}
