package com.wanxi.user.service.impl;

import com.wanxi.user.mapper.UserMapper;
import com.wanxi.cloud.pojo.User;
import com.wanxi.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    public User getPersonByName(String name) {
        return userMapper.getPersonByName(name);
    }

    public User getPersonById(Integer id) {
        return userMapper.getPersonById(id);
    }

    public List<User> getAllPerson() {
        return userMapper.getAllPerson();
    }

    public Integer addPerson(User person) {
        return userMapper.addPerson(person);
    }

    public Integer deletePerson(int id) {
        return userMapper.deletePerson(id);
    }

    public Integer updatePerson(User person) {
        return userMapper.updatePerson(person);
    }
}
