package com.wanxi.cloud.common;

public interface IErrorCode {

    long getCode();

    String getMessage();
}

