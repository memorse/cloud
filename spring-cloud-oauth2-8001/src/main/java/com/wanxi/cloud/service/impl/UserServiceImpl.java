package com.wanxi.cloud.service.impl;

import com.wanxi.cloud.mapper.UserMapper;
import com.wanxi.cloud.service.UserService;
import com.wanxi.cloud.utils.JwtTokenUtil;
import com.wanxi.cloud.vo.LoginParam;
import com.wanxi.cloud.vo.User;
import org.omg.CORBA.PRIVATE_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Override
    public String login(LoginParam loginParam) {
        loginParam.setPassword(passwordEncoder.encode(loginParam.getPassword()));
        User user = userMapper.loginForName(loginParam);
        Assert.isNull(user,"帐号获密码不存在");
        String token="";
        if (user!=null){
            token=jwtTokenUtil.generateToken(user);
        }
        return token;
    }
}
