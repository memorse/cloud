package com.wanxi.cloud.service;

import com.wanxi.cloud.mapper.PermissionMapper;
import com.wanxi.cloud.mapper.UserMapper;
import com.wanxi.cloud.utils.JwtTokenUtil;
import com.wanxi.cloud.vo.LoginParam;
import com.wanxi.cloud.vo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class UserDetailService implements UserDetailsService {
    private List<User> userList;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userMapper.getUserForName(username);

        Assert.isNull(user,"用户名或密码错误");
        User list = permissionMapper.getPermissionByName(user.getName());
        String[] split = list.getPers().split(",");
        HashSet hashSet=new HashSet(Arrays.asList(split));
        user.setAuthorities(hashSet);
        return user;
    }
}
