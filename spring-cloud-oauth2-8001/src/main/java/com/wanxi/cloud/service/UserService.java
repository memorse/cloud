package com.wanxi.cloud.service;

import com.wanxi.cloud.vo.LoginParam;
import com.wanxi.cloud.vo.User;

public interface UserService {
    String login(LoginParam loginParam);
}
