package com.wanxi.cloud.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Permission implements GrantedAuthority {
    private int id;
    private String name;
    private String code;
    private String uri;

    @Override
    public String getAuthority() {
        return this.code;
    }
}
