package com.wanxi.cloud;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
@SpringBootApplication
@EnableEurekaClient
@MapperScan("com.wanxi.cloud.mapper")
public class ServiceAuthAPP {
    public static void main(String[] args) {
        SpringApplication.run(ServiceAuthAPP.class,args);
    }
}
