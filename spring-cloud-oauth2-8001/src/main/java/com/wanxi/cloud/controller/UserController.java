package com.wanxi.cloud.controller;

import com.alibaba.fastjson.JSONObject;
import com.wanxi.cloud.common.CommonResult;
import com.wanxi.cloud.service.UserService;
import com.wanxi.cloud.utils.JwtTokenUtil;
import com.wanxi.cloud.vo.LoginParam;
import com.wanxi.cloud.vo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Value("${jwt.tokenHead}")
    private String tokenHead;
    @GetMapping("/getCurrentUser")
    public Object getCurrentUser(Authentication authentication) {
        return authentication.getPrincipal();
    }


    @Autowired
    private UserService userService;
    @PostMapping("/login")
    public CommonResult login(LoginParam loginParam){

        String token = userService.login(loginParam);
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("token",token);
        jsonObject.put("tokenHead",tokenHead);

        return CommonResult.success(jsonObject);

    }
}
