package com.wanxi.cloud.mapper;

import com.wanxi.cloud.pojo.Permission;
import com.wanxi.cloud.vo.User;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface PermissionMapper {
    @Select("select a.name,a.password,group_concat(per.code) as pers from person a " +
        "inner join person_permisson_ralation ppr on a.name=#{name} and a.id = ppr.person_id " +
        "inner join permission per on ppr.per_id=per.id;")
    User getPermissionByName(String name);
}
