package com.wanxi.cloud.mapper;

import com.wanxi.cloud.vo.LoginParam;
import com.wanxi.cloud.vo.User;
import org.apache.ibatis.annotations.Select;

public interface UserMapper {
    @Select("select name,password from person where name=#{name} and password=#{password}")
    User loginForName(LoginParam loginParam);
    @Select("select * from person where name=#{name}")
    User getUserForName(String userName);
}
