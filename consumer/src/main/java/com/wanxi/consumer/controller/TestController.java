package com.wanxi.consumer.controller;

import com.wanxi.cloud.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController

public class TestController {

    @Autowired
    private RestTemplate restTemplate;
    @Value("${service.url.user}")
    private String userServiceUrl;

    @GetMapping("/users")
    public List<User> getUser(){
        return restTemplate.getForObject(userServiceUrl+"/users", List.class);
    }

    @GetMapping("/feign/user/{id}")
    public User getUser(@PathVariable("id") Integer id){
        return restTemplate.getForObject(userServiceUrl+"/user/"+id,User.class);
    }
}
