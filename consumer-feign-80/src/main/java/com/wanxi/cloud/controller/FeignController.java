package com.wanxi.cloud.controller;


import com.wanxi.cloud.pojo.User;
import com.wanxi.cloud.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class FeignController {

    @Autowired
    private IUserService iUserService;

    @GetMapping("/feign/users")
    public List<User> getUsers(){
        return iUserService.getAllUser();
    }
    @GetMapping("/feign/user/{id}")
    public User getUser(@PathVariable("id") Integer id){
        return iUserService.getUser(id);
    }

}
